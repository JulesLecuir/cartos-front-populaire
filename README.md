# Cartographies du Nouveau Front Populaire

## Prise en main

### Local

```
npm i
npm run dev
```

### Deployer l'application

```
docker compose build
docker compose -f docker-compose.yml -f docker-compose.prod.yml --env-file docker/.env up
./scripts/compress_all_maps.sh
```

### Logs

```
docker compose -f docker-compose.yml -f docker-compose.prod.yml --env-file docker/.env logs -f --tail=1000
```

### Ajouter une nouvelle carte

Pour ajouter une nouvelle carte, suivez les étapes ci-dessous :

1. **Exécuter le script `scripts/add_new_map.sh`** :
   Utilisez le script `add_new_map.sh` pour ajouter votre nouvelle carte. Ce script va copier le fichier HTML de votre carte dans les dossiers appropriés et mettre à jour le dépôt git.

   ```bash
   # Assurez-vous que le script `scripts/add_new_map.sh` est exécutable en utilisant la commande suivante :
   chmod +x scripts/add_new_map.sh

   # Ensuite, exécutez le script avec les paramètres appropriés :
   ./add_new_map.sh <chemin_html> <nom_dossier> <nom_fichier>
   ```

   - `<chemin_html>` : Chemin vers le fichier HTML de la carte.
   - `<nom_dossier>` : Nom du dossier ou la carte sera stockée. Généralement, le numéro du département.
   - `<slug_de_la_carte>` : Nom du fichier HTML de la carte et de l'url.

   Exemple :

   ```bash
   ./scripts/add_new_map.sh path/to/your/map.html nom_du_dossier slug_de_la_carte
   ```

2. **Modifier le fichier `configuration.yml`** :
   Après avoir ajouté la carte avec le script, vous devez mettre à jour le fichier `configuration.yml` pour inclure les détails de la nouvelle carte ainsi que les datasets associés.

   Ouvrez `src/configuration.yml` et ajoutez une nouvelle entrée pour votre carte sous le département approprié. Voici un exemple de ce à quoi cela pourrait ressembler :

   ```yaml
   - codeDepartement: "XX"
     name: "Nom de la Carte"
     description: |
       Description de la carte.
     slug: "permalink-de-la-carte"
     datasets:
       - "dataset1"
       - "dataset2"
   ```

   - `codeDepartement` : Code du département.
   - `name` : Nom de la carte.
   - `description` : Description de la carte.
   - `slug` : Permalink de la carte.
   - `datasets` : Liste des datasets utilisés par la carte.

   Exemple :

   ```yaml
   - codeDepartement: "75"
     name: "Carte des arrondissements de Paris"
     description: |
       Carte détaillée des arrondissements de Paris.
     slug: "carte-arrondissements-paris"
     datasets:
       - "population"
       - "tauxDeChomage"
   ```

   Assurez-vous que tous les datasets utilisés par la carte sont listés dans le fichier `configuration.yml`. Si un dataset n'est pas listé, ajoutez-le en suivant le format approprié sous la section `datasets`. Par exemple, si votre carte utilise un dataset nommé "tauxDeChômage", et qu'il n'est pas dans `configuration.yml`, vous devriez l'ajouter comme suit :

   ```yaml
   tauxDeChomage:
     label: "Taux de chômage"
     description: |
       Pourcentage de la population active sans emploi.
   ```

3. **Vérifier et pousser les changements** :
   Assurez-vous que toutes les modifications sont correctes et poussez les changements vers le dépôt git.

   ```bash
   git add src/configuration.yml
   git commit -m "Ajout de la nouvelle carte pour le département XX"
   git push
   ```

En suivant ces étapes, vous pourrez ajouter une nouvelle carte à votre projet de manière structurée et cohérente.

### Recompresser toutes les cartes

Compresser les cartes HTML améliore la vitesse de chargement et économise la bande passante.

Pour recompresser toutes les cartes facilement, vous pouvez utiliser le script `recompress_all_maps.sh` disponible dans le projet. Ce script parcourt tous les fichiers HTML dans le dossier `build/maps` et ses sous-dossiers pour les recompresser en utilisant Brotli pour une meilleure efficacité de compression.

```bash
./scripts/compress_all_maps.sh
```

Assurez-vous que vous avez les permissions nécessaires pour exécuter le script (`sudo chmod u+x scripts/recompress_all_maps.sh`) et que Brotli est installé sur votre système (`sudo apt install brotli`).
