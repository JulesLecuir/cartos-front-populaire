import {useState} from "react";
import {Container, Stack} from "@mui/system";

export const CatGIF = () => {
  const [gifs] = useState(
    [
      "JIX9t2j0ZTN9S",
      "RU4KgvUGsj3eE",
      "10SAlsUFbyl5Dy",
      "pD83kYQkhuhgY",
      "xTiQygY6HW1GjoYKFq",
      "tBxyh2hbwMiqc",
      "a1kftfCMBOa0o",
      "lN9amhr8GZMhG",
      "5IAoDGpfh0c1y",
      "KijmmcKP62qWs",
    ].sort((a, b) => 0.5 - Math.random()),
  );

  return (
    <Container>
      <Stack alignItems={"center"} justifyContent={"center"}>
        <img
          alt="random cat gif"
          style={{
            borderRadius: 12,
            maxHeight: 300,
            minHeight: 300,
            margin: 20,
            width: "auto",
            height: "auto",
          }}
          src={`https://media.giphy.com/media/${gifs[0]}/giphy.gif`}
        />
      </Stack>
    </Container>
  );
};
