import {useEffect, useMemo, useState} from "react";
import {Card, Input, Checkbox, Tag, Alert} from "antd";
import {Typography} from "antd";
import {jauneThemeNFP} from "../app/theme";
import {Box, Container, Stack} from "@mui/system";
import {searchInObjectsList} from "../utils/searchInObjectsList";
import {groupBy} from "../utils/groupBy";
import {useWindowDimensions} from "../hooks/useWindowDimensions";
import {Link} from "react-router-dom";
import {Dataset, MapInfo} from "../app/types";
import {useGetConfiguration} from "../hooks/useGetConfiguration";
import {DatasetsList} from "../components/DatasetsList";
import Grid from "@mui/system/Unstable_Grid";
import {useGetSwingCircos} from "../hooks/useGetSwingCircos";
import {departements} from "../utils/departements";
import {SwingCircosInfo} from "../components/SwingCircosInfo";
import {logToBackend} from "../utils/logToBackend";
import {HTMLParagraph} from "../components/MultilineParagraph";
import {ReadOutlined, SearchOutlined} from "@ant-design/icons";
import {NFPColorBand} from "../components/NFPColorBand";
import {useDebounce} from "../hooks/useDebounce";
import {HideableContent} from "../components/HideableContent";
const {Title} = Typography;

export const MapsList = () => {
  useEffect(() => {
    logToBackend("Maps List");
  }, []);
  const {isMobileView} = useWindowDimensions();

  const {maps, datasets, global} = useGetConfiguration();
  const swingCircos = useGetSwingCircos();

  const [searchTerm, setSearchTerm] = useState(
    new URLSearchParams(window.location.search).get("search") || "",
  );

  const debouncedSetSearchTerm = useDebounce(setSearchTerm, 300);

  const sortedMaps = useMemo(
    () => maps.sort((a, b) => parseInt(a.codeDepartement) - parseInt(b.codeDepartement)),
    [maps],
  );

  const filteredMaps =
    searchInObjectsList(searchTerm, sortedMaps, (el) => [
      el.name,
      el.description,
      departements.find((dpt) => dpt.id === el.codeDepartement)?.label || "",
      ...(el.datasets?.map((dataset) => datasets[dataset]?.label) || []),
    ]) || [];

  const groupedSortedFilteredMapsEntries: Array<[string, MapInfo[]]> = Object.entries(
    groupBy(filteredMaps, (map) => map.codeDepartement),
  ).sort((a, b) => parseInt(a[0]) - parseInt(b[0]));

  return (
    <>
      <Box component={"header"} sx={{py: "2.5rem"}}>
        <Container maxWidth="lg" sx={{px: 3}}>
          <Title level={1} style={{color: "#e4032e", fontSize: "3.5rem"}}>
            Cartographies du
            <br /> Nouveau Front Populaire
          </Title>
        </Container>
      </Box>

      <NFPColorBand height={50} />

      <Container maxWidth="lg" sx={{py: 8, px: 2}}>
        <Stack gap={2}>
          {global.headline && (
            <Box mb={2} color={"grey"}>
              <HTMLParagraph value={global.headline} />
            </Box>
          )}

          <Card
            style={{
              position: "sticky",
              top: "0.5rem",
              zIndex: 1000,
              margin: "-0.5em",
              backgroundColor: jauneThemeNFP,

              marginBottom: "1rem",
              boxShadow: "0px 5px 10px rgba(0, 0, 0, 0.3)",
            }}>
            <Input
              suffix={<SearchOutlined />}
              autoFocus={!isMobileView}
              size="large"
              defaultValue={searchTerm}
              placeholder="Rechercher par département, titre ou description"
              onChange={(e) => debouncedSetSearchTerm(e.target.value)}
            />
          </Card>

          {global.modelsExplanation && (
            <HideableContent
              buttonOpenTitle={
                <>
                  <ReadOutlined /> À lire pour bien interpréter les cartes
                </>
              }>
              <HTMLParagraph value={global.modelsExplanation} />
            </HideableContent>
          )}

          <Alert
            type="success"
            showIcon
            icon={
              <div>
                <Tag color="success">NEW</Tag>
              </div>
            }
            message="Les cartes du second tour sont arrivées !"
            description={
              <>
                <span>Attention :</span> elles partent du principe que tous les candidats de la
                majorité présidentielle se sont retirés. Or, ce n'est peut-être pas (encore) le cas
                dans votre circo. Si vous voulez rappeler les derniers frondeurs à la raison,
                interpellez-les{" "}
                <a
                  href="https://shaketonpolitique.org/interpellations/elections-legislatives-2024/"
                  target="_blank">
                  ici
                </a>
                .
              </>
            }
          />

          {groupedSortedFilteredMapsEntries.length === 0 && (
            <div style={{color: "gray", textAlign: "center", marginTop: "2rem"}}>
              Chargement des cartes...
            </div>
          )}

          {groupedSortedFilteredMapsEntries.map(([codeDepartment, maps]) => (
            <div key={codeDepartment}>
              <Title level={2}>
                {departements.find((dep) => dep.id === codeDepartment)?.label}
              </Title>

              <Stack gap={2}>
                <SwingCircosInfo swingCircos={swingCircos} codeDepartement={codeDepartment} />

                {maps
                  .sort((a, b) => (a.outdated ? 1 : -1))
                  .map((mapInfo) => (
                    <Link to={`/${mapInfo.codeDepartement}/${mapInfo.slug}`} key={mapInfo.slug}>
                      <Card hoverable style={{opacity: mapInfo.outdated ? 0.5 : undefined}}>
                        <Title
                          level={4}
                          style={
                            mapInfo.outdated ? {marginTop: 0, marginBottom: 0} : {marginTop: 10}
                          }>
                          {mapInfo.name}
                        </Title>

                        {!mapInfo.outdated && (
                          <Grid container spacing={2}>
                            <Grid xs={12}>
                              <HTMLParagraph value={mapInfo.description} />
                            </Grid>

                            {mapInfo.datasets && (
                              <Grid xs={12}>
                                <HideableDatasetsList mapInfo={mapInfo} datasets={datasets} />
                              </Grid>
                            )}
                          </Grid>
                        )}
                      </Card>
                    </Link>
                  ))}
              </Stack>
            </div>
          ))}
        </Stack>
      </Container>
    </>
  );
};

const MapCardContent = ({
  mapInfo,
  datasets,
}: {
  mapInfo: MapInfo;
  datasets: Record<string, Dataset>;
}) => {
  const content = (
    <Grid container spacing={2}>
      <Grid xs={12}>
        <HTMLParagraph value={mapInfo.description} />
      </Grid>

      {mapInfo.datasets && (
        <Grid xs={12}>
          <HideableDatasetsList mapInfo={mapInfo} datasets={datasets} />
        </Grid>
      )}
    </Grid>
  );
  return mapInfo.outdated ? (
    <HideableContent buttonOpenTitle="Voir les infos" buttonCloseTitle="Masquer les infos">
      {content}
    </HideableContent>
  ) : (
    content
  );
};

const HideableDatasetsList = ({
  mapInfo,
  datasets,
}: {
  mapInfo: MapInfo;
  datasets: Record<string, Dataset>;
}) =>
  mapInfo.datasets ? (
    <HideableContent
      buttonOpenTitle="Masquer les ensembles de données"
      buttonCloseTitle="Afficher les ensembles de données">
      <DatasetsList mapDatasets={mapInfo.datasets} allDatasets={datasets} />
    </HideableContent>
  ) : null;
