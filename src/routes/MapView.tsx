import {useEffect, useState} from "react";
import {Button, Modal, ButtonProps, Collapse, Card, Alert} from "antd";
import {Typography} from "antd";
import {Container, Stack, Unstable_Grid as Grid} from "@mui/system";
import {useWindowDimensions} from "../hooks/useWindowDimensions";
import {useNavigate, useParams} from "react-router-dom";
import {useGetConfiguration} from "../hooks/useGetConfiguration";
import {
  ArrowLeftOutlined,
  EyeInvisibleOutlined,
  EyeOutlined,
  InfoCircleOutlined,
  ReadOutlined,
} from "@ant-design/icons";
import {DatasetsList} from "../components/DatasetsList";
import {logToBackend} from "../utils/logToBackend";
import {HTMLParagraph} from "../components/MultilineParagraph";
import {CatGIF} from "../CatGIF";
import {MapNotFoundScreen} from "../components/MapNotFoundScreen";
import {NFPColorBand} from "../components/NFPColorBand";
import {layersButtonsStyles} from "../app/mapIframeStyles";
import {LegendCard} from "../components/LegendCard";
import {departements} from "../utils/departements";

const {Title} = Typography;

export const MapView = () => {
  const navigate = useNavigate();
  const {codeDepartement, slug} = useParams();
  const {maps, datasets, global} = useGetConfiguration();
  const [isMapInfoModalVisible, setIsMapInfoModalVisible] = useState(false);
  const [isHeaderVisible, setIsHeaderVisible] = useState(true);
  const {isMobileView} = useWindowDimensions();

  useEffect(() => {
    logToBackend("Map View", {value: `${codeDepartement}/${slug}`});
  }, [codeDepartement, slug]);

  const [timePageLoad] = useState(new Date());

  useEffect(() => {
    const styleElement = document.createElement("style");
    styleElement.innerHTML = layersButtonsStyles;
    function tryIt() {
      try {
        document.querySelector("iframe")?.contentWindow?.document.body.appendChild(styleElement);
      } catch (err) {
        console.error("could not customize iframe style (retry in 0.5s)", err);
        setTimeout(tryIt, 500);
      }
    }

    setTimeout(tryIt, 500);
  }, [isMobileView]);

  const selectedMap = maps.find(
    (map) => map.codeDepartement === codeDepartement && map.slug === slug,
  );

  // Map not found : return the 404 screen
  if (!selectedMap) {
    return maps?.length > 0 ? <MapNotFoundScreen /> : null;
  }

  const showModal = () => {
    setIsMapInfoModalVisible(true);
    logToBackend("Open Infos Modal");
  };
  const handleOk = () => setIsMapInfoModalVisible(false);
  const handleCancel = () => setIsMapInfoModalVisible(false);

  const DescriptionModalButton = () => {
    return (
      <>
        <Button
          icon={<InfoCircleOutlined />}
          type="primary"
          size="large"
          onClick={showModal}
          style={{fontWeight: "bold", flexShrink: 0}}>
          {!isMobileView && "Infos"}
        </Button>

        <Modal
          title="Informations sur la carte"
          width={"max(70vw, 800px)"}
          open={isMapInfoModalVisible}
          onOk={handleOk}
          footer={null}
          onCancel={handleCancel}>
          <Grid container spacing={4}>
            <Grid xs={12}>
              {global.modelsExplanation && (
                <Collapse
                  size="large"
                  items={[
                    {
                      label: (
                        <>
                          <ReadOutlined /> À lire pour bien interpréter les cartes
                        </>
                      ),
                      children: <HTMLParagraph value={global.modelsExplanation} />,
                    },
                  ]}
                />
              )}
            </Grid>

            <Grid xs={12}>
              <Card title="Description">
                <HTMLParagraph value={selectedMap.description} />
              </Card>
            </Grid>

            {selectedMap?.datasets && (
              <Grid xs={12}>
                <Card title="Ensembles de données">
                  <DatasetsList
                    mapDatasets={selectedMap.datasets}
                    allDatasets={datasets}
                    expanded
                  />
                </Card>
              </Grid>
            )}
          </Grid>
        </Modal>
      </>
    );
  };

  const HeaderButton = (props: ButtonProps) => <Button type="text" {...props} size="large" />;

  return (
    <>
      {/* Show / Hide header button */}
      <HeaderButton
        onClick={() => {
          setIsHeaderVisible((prev) => !prev);
          logToBackend(isHeaderVisible ? "Click Hide Header Button" : "Click Show Header Button");
        }}
        title={isHeaderVisible ? "Cacher l'en-tête" : "Afficher l'en-tête"}
        style={{
          background: isMobileView || isHeaderVisible ? undefined : "white",
          position: "absolute",
          top: !isMobileView && isHeaderVisible ? "0.5rem" : "calc(100vh - 3.2rem)", // Déplace le bouton en bas si l'en-tête est caché
          right: "1rem",
        }}
        icon={isHeaderVisible ? <EyeInvisibleOutlined /> : <EyeOutlined />}>
        {!isMobileView && (isHeaderVisible ? "Cacher" : "Afficher")}
      </HeaderButton>

      {/* Header */}
      <header style={{display: isHeaderVisible ? undefined : "none"}}>
        <Container maxWidth="lg">
          <Stack direction="row" gap={2} justifyContent="space-between" alignItems="center">
            <Stack direction="row" gap={2} justifyContent="space-between" alignItems="center">
              {/* Back button */}
              <HeaderButton icon={<ArrowLeftOutlined />} onClick={() => navigate("/")} />

              {/* Map title */}
              <Title level={1} style={{fontSize: "1.5rem", lineHeight: "1.6rem"}}>
                {
                  departements.find(
                    (departement) => departement.id === selectedMap?.codeDepartement,
                  )?.label
                }{" "}
                <span style={{fontWeight: 500}}>- {selectedMap?.name}</span>
              </Title>
            </Stack>

            {/* Map description */}
          </Stack>
        </Container>
      </header>

      <NFPColorBand height={10} />

      {selectedMap.outdated && (
        <Alert
          type="info"
          banner
          closable
          message={
            <div style={{textAlign: "center", fontWeight: "bold", fontSize: 18}}>
              Une nouvelle carte mise à jour existe pour ce département.{" "}
              <a href={`/?search=${selectedMap.codeDepartement}`}>Voir la nouvelle carte</a>
            </div>
          }
        />
      )}

      {/* Content */}
      <Stack
        direction="column"
        justifyContent="center"
        alignItems="center"
        style={{width: "100%", height: "100vh"}}>
        {/* Loading screen */}
        <Stack id="loadingMessage" style={{position: "absolute", textAlign: "center"}} gap={3}>
          <div>
            <Title level={5}>Chargement de la carte...</Title>
            <Typography>
              Oui, ça peut prendre un peu de temps !
              <br />
              Mais on a un GIF de chat pour toi.
            </Typography>
          </div>
          <CatGIF />
        </Stack>

        {/* Map */}
        <iframe
          title="carte"
          src={`/embed-maps/${selectedMap.codeDepartement}/${selectedMap.slug}.html`}
          style={{
            width: "100%",
            height: "100%",
            border: "none",
          }}
          onLoad={() => {
            const loadingMessage = document.getElementById("loadingMessage");
            if (loadingMessage) {
              loadingMessage.style.display = "none";
            }
            const timePageLoadFinished = new Date();
            logToBackend(`Map Load Time (s) - ${selectedMap.codeDepartement}/${selectedMap.slug}`, {
              value: (timePageLoadFinished.getTime() - timePageLoad.getTime()) / 1000,
              type: "arrayOfValues",
            });
          }}
        />

        <Stack
          sx={{
            position: "fixed",
            bottom: 20,
            left: 20,
            width: 300,
            zIndex: 1000,
          }}
          direction="row"
          alignItems="end"
          gap={2}>
          <LegendCard />
          <DescriptionModalButton />
        </Stack>
      </Stack>
    </>
  );
};
