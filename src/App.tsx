import {Route, Routes} from "react-router-dom";
import {MapsList} from "./routes/MapsList";
import {MapView} from "./routes/MapView";
import {Layout} from "antd";

function App() {
  return (
    <Layout style={{height: "100vh", overflow: "auto"}}>
      <Routes>
        <Route path="/" element={<MapsList />} />
        <Route path="/:codeDepartement/:slug" element={<MapView />} />
      </Routes>
    </Layout>
  );
}

export default App;
