import {Stack} from "@mui/system";
import {Tag} from "antd";
import {Dataset} from "../app/types";
import {HTMLParagraph} from "./MultilineParagraph";

export const DatasetsList = ({
  mapDatasets,
  allDatasets = {},
  expanded = false,
}: {
  mapDatasets: string[];
  allDatasets: Record<string, Dataset>;
  expanded?: boolean;
}) => {
  return (
    <Stack
      direction={expanded ? "column" : "row"}
      gap={1}
      flexWrap={"wrap"}
      alignItems={"flex-start"}>
      {mapDatasets.map((dataset) =>
        allDatasets[dataset] ? (
          <>
            <Tag
              key={dataset}
              title={!expanded ? allDatasets[dataset].description : undefined}
              color={allDatasets[dataset].color}>
              {allDatasets[dataset].label}
            </Tag>
            {expanded && (
              <HTMLParagraph
                style={{marginLeft: "1rem"}}
                value={allDatasets[dataset].description}
              />
            )}
          </>
        ) : null,
      )}
    </Stack>
  );
};
