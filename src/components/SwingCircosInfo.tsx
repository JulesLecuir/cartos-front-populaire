import {Alert, Card} from "antd";
import {MapInfo, SwingCirco} from "../app/types";
import Paragraph from "antd/es/typography/Paragraph";
import {Stack} from "@mui/system";
import Title from "antd/es/typography/Title";
import {logToBackend} from "../utils/logToBackend";

export const SwingCircosInfo = ({
  swingCircos,
  codeDepartement,
}: {
  swingCircos: SwingCirco[];
  codeDepartement: MapInfo["codeDepartement"];
}) => {
  const swingCircosForDepartment = swingCircos.filter((circo) => {
    return circo.codeDepartement === codeDepartement;
  });
  return swingCircosForDepartment.length > 0 ? (
    <Alert
      type="info"
      message={"Des circos ont besoin d'aide dans ce département !"}
      description={
        <>
          <Stack gap={2}>
            <Paragraph style={{margin: 0, color: "gray", fontSize: 14}}>
              Ces circos ont été classées comme prioritaires par le site{" "}
              <a
                href={"https://indivisibles.fr/5jourspourgagner/"}
                target="_blank"
                onClick={() => logToBackend("Click SwingCirco New", {value: codeDepartement})}>
                indivisibles.fr
              </a>
              .
            </Paragraph>
            <Stack direction="row" gap={2} flexWrap="wrap">
              {swingCircosForDepartment.map((swingCirco) => (
                <Card
                  key={swingCirco.Nom}
                  size="small"
                  style={{flexBasis: "45%", flexGrow: 1, minWidth: "fit-content", border: "none"}}>
                  {" "}
                  <Title level={5} style={{marginTop: 0}}>
                    {swingCirco.Prénom} {swingCirco.Nom}
                  </Title>
                  <Paragraph style={{margin: 0}}>
                    Circonscription n°{swingCirco["N° Circo"]}
                  </Paragraph>
                </Card>
              ))}
            </Stack>
            <a
              href={"https://indivisibles.fr/5jourspourgagner/"}
              target="_blank"
              onClick={() => logToBackend("Click SwingCirco New", {value: codeDepartement})}>
              Je veux aider, je me rends sur <u>indivisibles.fr</u> !
            </a>
          </Stack>
        </>
      }
      showIcon
    />
  ) : null;
};
