import {Box, Stack} from "@mui/system";
import {Collapse, Typography} from "antd";

export const LegendCard = () => {
  const LegendEntry = ({color, title}: {color: string; title: string}) => {
    return (
      <Stack direction="row" alignItems="baseline" gap={1}>
        <Box
          style={{
            flexShrink: 0,
            backgroundColor: color,
            width: 14,
            height: 14,
          }}
        />
        <Typography>{title}</Typography>
      </Stack>
    );
  };

  return (
    <Collapse
      defaultActiveKey={"open"}
      size="small"
      style={{marginTop: 1, marginBottom: -2}}
      items={[
        {
          key: "open",
          label: "Légende",
          children: (
            <Stack gap={0.6}>
              <LegendEntry color="#e60000" title="Front populaire" />
              <LegendEntry color="#ffcc00" title="Macronie" />
              <LegendEntry
                color="#542788"
                title="Divers (LR, AR, Animaliste, écologie au centre)"
              />
              <LegendEntry color="#996633" title="Extrême droite" />
              <LegendEntry color="#bababa" title="Abstention" />
            </Stack>
          ),
        },
      ]}
    />
  );
};
