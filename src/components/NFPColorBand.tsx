import {Box, Stack} from "@mui/system";

export const NFPColorBand = ({height}: {height: number}) => {
  return (
    <Box sx={{minHeight: height, overflow: "hidden"}}>
      <Stack
        direction="row"
        sx={{height: "100%", width: "105%", marginLeft: "-2.5%"}}
        alignItems={"stretch"}
        justifyContent={"space-between"}>
        <Box sx={{bgcolor: "#37ae6d", transform: "skew(-20deg)", flexGrow: 1}}></Box>
        <Box sx={{bgcolor: "#e4032e", transform: "skew(-20deg)", flexGrow: 1}}></Box>
        <Box sx={{bgcolor: "#ffed00", transform: "skew(-20deg)", flexGrow: 1}}></Box>
        <Box sx={{bgcolor: "#951b81", transform: "skew(-20deg)", flexGrow: 1}}></Box>
        <Box sx={{bgcolor: "#e50053", transform: "skew(-20deg)", flexGrow: 1}}></Box>
      </Stack>
    </Box>
  );
};
