import {ArrowDownOutlined, ArrowUpOutlined} from "@ant-design/icons";
import {Button} from "antd";
import {useState} from "react";
import {Collapse as CollapseEffect} from "@mui/material";
import {Box} from "@mui/system";

export const HideableContent = ({
  buttonOpenTitle,
  buttonCloseTitle = buttonOpenTitle,
  children,
}: {
  buttonOpenTitle: React.ReactNode;
  buttonCloseTitle?: React.ReactNode;
  children: React.ReactNode;
}) => {
  const [open, setOpen] = useState(false);

  const handleToggle = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    setOpen(!open);
  };

  return (
    <>
      <Button
        type="link"
        style={{paddingLeft: 0}}
        onClick={handleToggle}
        icon={open ? <ArrowUpOutlined /> : <ArrowDownOutlined />}>
        {open ? buttonOpenTitle : buttonCloseTitle}
      </Button>
      <CollapseEffect in={open}>
        <Box mt={1}>{children}</Box>
      </CollapseEffect>
    </>
  );
};
