import {HTMLAttributes} from "react";

export const HTMLParagraph = ({
  value,
  ...props
}: {value?: string} & HTMLAttributes<HTMLDivElement>) => {
  return value ? <div dangerouslySetInnerHTML={{__html: value}} {...props} /> : null;
};
