import {ArrowLeftOutlined} from "@ant-design/icons";
import {Stack} from "@mui/system";
import {Button, Result} from "antd";
import {useNavigate} from "react-router-dom";

export const MapNotFoundScreen = () => {
  const navigate = useNavigate();
  return (
    <Stack
      direction="column"
      justifyContent="center"
      alignItems="center"
      style={{width: "100%", height: "100vh"}}>
      <Result
        status="404"
        title="Oups !"
        subTitle="La carte que vous cherchez n'existe pas."
        extra={
          <Button
            type="primary"
            size="large"
            onClick={() => navigate("/")}
            icon={<ArrowLeftOutlined />}>
            Retour à la page d'accueil
          </Button>
        }
      />
    </Stack>
  );
};
