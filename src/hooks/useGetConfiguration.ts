import axios from "axios";
import {Configuration, MapInfo} from "../app/types";
import {useEffect, useState} from "react";
import configurationMock from "../configurationMock.json";

export const useGetConfiguration = () => {
  const [configuration, setConfiguration] = useState<Configuration>({
    maps: [],
    datasets: {},
    global: {
      defaultMapsProps: {},
      modelsExplanation: "",
    },
  });

  useEffect(() => {
    const fetchConfiguration = async () => {
      // Ici, on simule une récupération de données avec une promesse résolue directement
      const configData: Configuration = (
        await axios.get("/api/configuration").catch(() => ({data: configurationMock}))
      ).data; // In local dev, get the conf file directly

      configData.maps = configData.maps.map(
        (map) =>
          ({
            ...map,
            name: map.name ?? configData.global.defaultMapsProps[map.slug].name,
            description:
              map.description ?? configData.global.defaultMapsProps[map.slug].description,
            datasets: map.datasets ?? configData.global.defaultMapsProps[map.slug].datasets,
            outdated: map.outdated ?? configData.global.defaultMapsProps[map.slug].outdated,
          }) satisfies MapInfo as MapInfo,
      );

      setConfiguration(configData);
    };

    fetchConfiguration();
  }, []);

  return configuration;
};
