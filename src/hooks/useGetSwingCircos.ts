import {useEffect, useState} from "react";
import {SwingCirco} from "../app/types";
import axios from "axios";
import swingCircosMock from "../swingCircosMock.json";

export const useGetSwingCircos = () => {
  const [swingCircos, setSwingCircos] = useState<SwingCirco[]>([]);

  useEffect(() => {
    // Simuler la récupération des données
    const fetchSwingCircos = async () => {
      // Ici, on simule une récupération de données avec une promesse résolue directement
      const data: SwingCirco[] = (await axios.get("/api/swing-circos").catch(() => swingCircosMock))
        .data;
      setSwingCircos(
        data.map((swingCirco) => ({
          ...swingCirco,
          codeDepartement: swingCirco["Code Dpt"].padStart(2, "0"),
        })),
      );
    };

    fetchSwingCircos();
  }, []);

  return swingCircos;
};
