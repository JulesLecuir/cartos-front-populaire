import React, {Suspense} from "react";
import ReactDOM from "react-dom";
import {BrowserRouter} from "react-router-dom";
import {App, ConfigProvider} from "antd";
import frFR from "antd/es/locale/fr_FR";
import {LIGHT_THEME} from "./app/theme";
import {WindowDimensionsProvider} from "./hooks/useWindowDimensions";
import "./app/styles.css";

import CartoApp from "./App";

const Root: React.FC = () => {
  return (
    <Suspense fallback={null}>
      <BrowserRouter>
        <WindowDimensionsProvider>
          <ConfigProvider locale={frFR} theme={LIGHT_THEME}>
            <App>
              <CartoApp />
            </App>
          </ConfigProvider>
        </WindowDimensionsProvider>
      </BrowserRouter>
    </Suspense>
  );
};

ReactDOM.render(<Root />, document.getElementById("app-root"));
