import {ThemeConfig} from "antd";

export const jauneTheme = "#FFED00";
export const beigeThemeLight = "#ffffff";

export const vertThemeNFP = "#37ae6d";
export const rougeThemeNFP = "#E4032E";
export const jauneThemeNFP = "#ffed00";
export const violetThemeNFP = "#951b81";
export const roseThemeNFP = "#e50053";

export const LIGHT_THEME: ThemeConfig = {
  token: {
    colorPrimary: "black",
    colorLink: violetThemeNFP,
    colorTextPlaceholder: "grey",
    colorTextBase: "#000000",
    borderRadius: 0,
    colorBgContainer: "#ffffff",
    colorBgElevated: "#fafafa",
    colorBgLayout: "#f8f8f8",
    colorBorder: "black",
    colorBorderSecondary: "grey",
    wireframe: false,
    fontSize: 16,
  },
  components: {
    Card: {
      fontWeightStrong: 500,
    },
    Modal: {
      fontWeightStrong: 500,
      wireframe: true,
    },
    Collapse: {
      headerBg: "black",
      colorTextHeading: "white",
      colorBorder: "#c0c0c0",
    },
  },
};
