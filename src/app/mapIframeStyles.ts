export const layersButtonsStyles = `
.leaflet-control-layers-separator, .leaflet-control-layers-base {
    display: none;
}

.leaflet-control-layers {
    position: absolute; 
    top: 10px; 
    right: 10px;
    border-radius: 0;
    border-none;
}
.leaflet-control-layers:not(.leaflet-control-layers-expanded){
    background: black;
 }
.leaflet-control-layers-toggle {
    min-width: max(3vw, 50px);
    min-height: max(3vw, 50px);
    background-size: 65%;   
}
.leaflet-control-layers-overlays {}
.leaflet-control-layers.leaflet-control-layers-expanded {
    padding: 15px;
}

.leaflet-control-layers-overlays > label > span > span {
    font-size: 14px;
    font-weight: normal !important;
    font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,'Noto Sans',sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';
    margin-left: 0.5rem;
}


.legend.leaflet-control {
    display: none;
}

body > div:first-child {
    display: none;
}
`;
