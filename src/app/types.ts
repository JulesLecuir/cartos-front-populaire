export type Dataset = {
  label: string;
  description: string;
  color?: string;
};

export type MapInfo = {
  codeDepartement: string;
  name: string;
  outdated?: boolean;
  description: string;
  slug: string;
  datasets: Array<string>;
};

export type Configuration = {
  maps: MapInfo[];
  datasets: Record<string, Dataset>;
  global: {
    headline?: string;
    modelsExplanation: string;
    defaultMapsProps: Record<string, Partial<MapInfo>>;
  };
};

export type SwingCirco = {
  Circo: string;
  "Nom Dpt": string;
  "Code Dpt": string;
  codeDepartement: string;
  "N° Circo": string;
  "#Circo": string;
  Nom: string;
  Prénom: string;
  Statut: string;
};
