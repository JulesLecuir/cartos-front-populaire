export const logToBackend = (
  log: string,
  {
    value = "default",
    type = "count",
  }: {
    value?: string | number;
    type?: "count" | "arrayOfValues";
  } = {},
) => {
  fetch(`/api/logging?log=${log}&type=${type}&value=${value}`);
};
