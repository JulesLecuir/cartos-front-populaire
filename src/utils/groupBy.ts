export const groupBy = <T, K extends string | number>(
  array: T[],
  getKey: (item: T) => K,
): Record<K, T[]> => {
  return array.reduce<Record<K, T[]>>(
    (result, currentValue: T) => {
      const groupKey = getKey(currentValue);
      (result[groupKey] = result[groupKey] || []).push(currentValue);
      return result;
    },
    {} as Record<K, T[]>,
  );
};
