export const truncate = (str: string, n: number) =>
  str.replace(new RegExp("(.{" + n + "})..+"), "$1...");

export const capitalize = (str: string) => str.charAt(0).toUpperCase() + str.slice(1);

export const isValidObjectId = (id: string) => !!/[0-9a-z]{24}/.exec(id);

export const normalize = (str?: Object | null, caseSensitive = false) => {
  const normalized =
    str
      ?.toString() // Must be done if the string is actually a number
      .normalize("NFD")
      .replace(/[\u0300-\u036f]/g, "") || "";
  return caseSensitive ? normalized : normalized.toLowerCase();
};

const DEFAULT_DELIMITERS = {start: "{{", end: "}}"};

/**
 * Replaces some strings in a text
 * @param text the initial text
 * @param valuesToReplace a key/value hash of values to match and replace
 * @param delimiter by default, will be {start: "{{", end: "}}"}. If false, no delimiter at all.
 * @returns {string}
 */
export const replaceInText = (
  text: string,
  valuesToReplace: Record<string, string>,
  delimiter: false | {start?: string; end?: string} = DEFAULT_DELIMITERS,
) => {
  if (!text || text === "") return;

  for (const [match, replace] of Object.entries(valuesToReplace)) {
    text = text.replaceAll(
      delimiter
        ? `${delimiter.start || DEFAULT_DELIMITERS.start}${match}${
            delimiter.end || DEFAULT_DELIMITERS.end
          }`
        : match,
      replace,
    );
  }
  return text;
};
