const compressMiddleware = (contentType) => (req, res, next) => {
  if (req.headers["accept-encoding"]?.includes("br")) {
    // Brotli is the most efficient compression
    req.url = req.url + ".br";
    res.set("Content-Encoding", "br");
  } else if (req.headers["accept-encoding"]?.includes("gzip")) {
    // Otherwise do Gzip
    req.url = req.url + ".gz";
    res.set("Content-Encoding", "gzip");
  }

  // Force content mime type to JS for the browser https://stackoverflow.com/a/60759499
  res.set("Content-Type", contentType);

  // Go to somewhere else
  next();
};

module.exports = compressMiddleware;
