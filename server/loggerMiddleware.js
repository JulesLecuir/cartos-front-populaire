const fs = require("fs");
const path = require("path");
const crypto = require("crypto");

// Create the logs directory if it does not exist
if (!fs.existsSync(path.resolve(__dirname, "../logs"))) {
  fs.mkdirSync(path.resolve(__dirname, "../logs"), {recursive: true});
}

const statsFilePath = path.resolve(__dirname, "../logs/stats.json");

// Retrieve existing stats from file
let stats = {total: {}, byDate: {}, perUser: {}};
try {
  if (fs.existsSync(statsFilePath, "utf8")) {
    const data = fs.readFileSync(statsFilePath, "utf8");
    stats = JSON.parse(data);
  }
} catch (err) {
  console.error("Erreur lors de la lecture du fichier stats.json", err);
}

const loggerMiddleware = (req, res) => {
  const dateString = new Date().toLocaleDateString("fr-FR", {
    day: "2-digit",
    month: "2-digit",
    year: "numeric",
  });
  const timeString = new Date().toLocaleTimeString("fr-FR", {
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
  });

  try {
    const {type, log, value} = req.query || {};
    const ipAddress = req.headers["x-real-ip"] || req.headers["x-forwarded-for"] || req.ipp;
    const ipAdressAnonymized = ipAddress
      ? crypto.createHash("sha256").update(ipAddress).digest("hex").slice(0, 7)
      : "unknown";

    // Log stuff
    const logLine =
      `${dateString} ${timeString} | ${ipAdressAnonymized} | ${log.padEnd(17, " ")}` +
      (value !== "default" ? ` - ${value}` : "");
    console.log(logLine);
    // Log the line and add it to a log file (by date)
    fs.appendFileSync(
      path.resolve(__dirname, `../logs/${dateString.replace(/\//g, "-")}.txt`),
      `${logLine}\n`,
      "utf8",
    );

    // Update stats by day, then by log name and type
    stats.byDate[dateString] ||= {};
    if (type === "count") {
      stats.byDate[dateString][log] ||= {};
      stats.total[log] ||= {};
      stats.byDate[dateString][log][value] = (stats.byDate[dateString][log][value] || 0) + 1;
      stats.byDate[dateString][log].total = (stats.byDate[dateString][log].total || 0) + 1;
      stats.total[log][value] = (stats.total[log][value] || 0) + 1;
    } else if (type === "arrayOfValues") {
      stats.byDate[dateString][log] = [...(stats.byDate[dateString][log] || []), value];
      stats.total[log] = [...(stats.total[log] || []), value];
    }

    // Update stats for unique users
    stats.byDate[dateString].uniqueUsers ||= {};
    stats.total.uniqueUsers ||= {};
    stats.byDate[dateString].uniqueUsers[ipAdressAnonymized] =
      (stats.byDate[dateString].uniqueUsers[ipAdressAnonymized] || 0) + 1;
    stats.byDate[dateString].uniqueUsers.total =
      Object.keys(stats.byDate[dateString].uniqueUsers).length - 1;
    stats.total.uniqueUsers[ipAdressAnonymized] =
      (stats.total.uniqueUsers[ipAdressAnonymized] || 0) + 1;
    stats.total.uniqueUsers.total = Object.keys(stats.total.uniqueUsers).length - 1;

    // Write stats to file even if it does not exist yet
    fs.writeFileSync(statsFilePath, JSON.stringify(stats, null, 2), "utf8");
  } catch (e) {
    console.error("ERROR during logging", e);
  }

  res.sendStatus(200);
};

module.exports = loggerMiddleware;
