const getSwingCircos = async () => {
  const indivisibleUrl = "https://indivisibles.fr/5jourspourgagner/actions.json";

  try {
    const response = await fetch(indivisibleUrl);
    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Erreur lors de la récupération des données des circonscriptions swing:", error);
    throw error;
  }
};

module.exports = getSwingCircos;
