const markdownit = require("markdown-it");
const fs = require("fs");
const yaml = require("js-yaml");
const path = require("path");

const md = markdownit();

const DEV_MODE = process.env.NODE_ENV === "development";

const parseYamlFile = (filePath) => {
  const fileContents = fs.readFileSync(filePath, "utf8");
  return yaml.load(fileContents);
};

const getConfiguration = async () => {
  // Get the data from file
  const configData = parseYamlFile(
    path.resolve(__dirname, DEV_MODE ? "../configuration.example.yml" : "../configuration.yml"),
  );

  // Convert to markdown what needs to be converted
  return {
    maps: configData.maps.map((map) => ({
      ...map,
      description: map.description ? md.render(map.description) : undefined,
    })),
    datasets: Object.fromEntries(
      Object.entries(configData.datasets).map(([key, dataset]) => [
        key,
        {...dataset, description: md.render(dataset.description)},
      ]),
    ),
    global: Object.fromEntries(
      Object.entries(configData.global).map(([key, value]) => [
        key,
        typeof value === "string" ? md.render(value) : value,
      ]),
    ),
  };
};

module.exports = getConfiguration;
