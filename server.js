const express = require("express");
const path = require("path");
const PORT = process.env.PORT || 2000;
const helmet = require("helmet");
const app = express();
const loggerMiddleware = require("./server/loggerMiddleware");
const getSwingCircos = require("./server/getSwingCircos");
const compressMiddleware = require("./server/compressMiddleware");
const getConfiguration = require("./server/getConfiguration");

/**
 * SECURITY
 */

app.use(helmet({contentSecurityPolicy: false}));
app.disable("x-powered-by");

/**
 * COMPRESSED APP ARTEFACTS SERVE
 */

// Redirect all JS, CSS, and HTML files to their compressed variant, so the download is quicker
app.get("*.js", compressMiddleware("application/javascript"));
app.get("*.css", compressMiddleware("text/css"));
app.get("*.html", compressMiddleware("text/html"));

/**
 * SWING CIRCOS & CONFIGURATION
 */

app.get("/api/swing-circos", (req, res) => {
  getSwingCircos().then((circos) =>
    res.json(circos.data.filter((circo) => circo.Statut === "Prioritaire")),
  );
});

app.get("/api/configuration", (req, res) => {
  getConfiguration().then((config) => res.json(config));
});

/**
 * LOGGING
 */

app.get("/api/logging", loggerMiddleware);

/**
 * STATIC FILES
 */

// Static resources should just be served as they are
app.use("/embed-maps", express.static("maps", {maxAge: "4h"}));
app.use(express.static("build", {maxAge: "4h"}));

/**
 * THE REST
 */

// Serve the index file
app.use((req, res) => {
  res.sendFile(path.resolve(__dirname, "build", "index.html"));
});

// Global error Handler
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send("Something broke, oops!");
});

/**
 * START SERVER
 */

// Set up the server to listen on the right port
app.listen(PORT, (error) => {
  if (error) return console.log("Error during app startup", error);
  console.log("Dynamic server listening on " + PORT + "...");
});
