#!/bin/bash

current_dir="$(dirname "$0")"

# # Vérifier si les arguments ont été donnés via la commande
# if [ -z "$1" ]; then
#     while true; do
#         read -p "Entrez le chemin du fichier HTML : " chemin_html
#         if [ -f "$chemin_html" ]; then
#             break
#         else
#             echo "Le fichier n'existe pas, veuillez réessayer. Utilisez un chemin relatif à votre dossier courant."
#         fi
#     done
# else
#     chemin_html="$1"
# fi

# if [ -z "$2" ]; then
#     read -p "Entrez le code du département de la carte (ou 'natio' si la carte est nationale) : " nom_dossier
# else
#     nom_dossier="$2"
# fi

# if [ -z "$3" ]; then
#     read -p "Entrez le slug de la carte (la carte sera à l'adresse $nom_dossier/[slug]) : " slug_carte
# else
#     slug_carte="$3"
# fi

chemin_html=analyse_euro24_"$1".html
slug_carte=carte-complete-par-bureaux-de-vote-europeennes-2024
nom_dossier="$1"


echo $chemin_html
echo $nom_dossier
echo $slug_carte

dossier_public_maps="$current_dir"/../public/maps/"$nom_dossier"
dossier_build="$current_dir"/../build/maps/"$nom_dossier"

# Créer le dossier si nécessaire
mkdir -p "$dossier_public_maps"
mkdir -p "$dossier_build"

# Copier et renommer le fichier HTML dans le dossier spécifié
mv "$chemin_html" "$dossier_public_maps"/"$slug_carte".html

brotli --best "$dossier_public_maps"/"$slug_carte".html

# Copier la carte et la care compressée dans 
cp "$dossier_public_maps"/"$slug_carte".html "$dossier_build"/"$slug_carte".html
cp "$dossier_public_maps"/"$slug_carte".html.br "$dossier_build"/"$slug_carte".html.br

# echo ""
# echo "Carte ajoutée et compressée avec succès."
# echo ""
# echo "Ajoutez la configuration suivante à votre fichier src/configuration.yml sous le département approprié. Les champs entre < > doivent être remplis :"
# echo ""
# echo "- name: \"<nom>\""
# echo "  description: |"
# echo "    <description>"
# echo "  slug: \"$slug_carte\""
# echo "  datasets:"
# echo "    - \"<dataset1>\""
# echo "    - \"<dataset2>\""
# echo "    - \"<dataset3>\""
# echo ""
# echo "Une fois la configuration ajoutée, commitez et poussez les changements :"
# echo ""
# echo "git add src/configuration.yml"
# echo "git commit -m \"Ajout configuration nouvelle carte : $nom_dossier/$slug_carte\""
# echo "git push"
