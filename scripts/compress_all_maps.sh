#!/bin/bash

current_dir="$(dirname "$0")"

# Re-compresser toutes les cartes présentes dans build/maps et tous ses sous-dossiers

# La commande 'find' est utilisée pour rechercher tous les fichiers HTML dans le dossier 'build/maps' et tous ses sous-dossiers.
# L'option '-type f' spécifie que nous recherchons des fichiers.
# L'option '-name "*.html"' spécifie que nous recherchons des fichiers qui se terminent par '.html'.
find "$current_dir"/../build/maps -type f -name "*.html" | while IFS= read -r fichier_html; do
  echo "Compressing $fichier_html"
  
  # Déterminer le répertoire du fichier HTML
  repertoire=$(dirname "$fichier_html")
  
  # Supprimer le fichier compressé s'il existe déjà pour éviter les doublons
  if [ -f "$repertoire/$(basename "$fichier_html.br")" ]; then
    echo " > File $fichier_html.br already exists, skipping compression."
  fi

  # Pour chaque fichier trouvé, nous utilisons la commande 'brotli' pour le compresser.
  # L'option '--best' indique que nous voulons la meilleure compression possible, bien que cela puisse prendre plus de temps.
  brotli --best "$fichier_html"
done

echo "Toutes les cartes ont été compressées avec succès."

